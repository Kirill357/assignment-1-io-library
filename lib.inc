section .text

%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define STDOUT 1
%define ONE_SYMBOL 1
%define TEN_SN 10
%define STRING_END 0
%define ZERO_ASCII 48
%define MINUS_ASCII '-'
%define READ_SYSCALL 0
%define STDIN 0
%define SPACE_ASCII ` `
%define NEWLINE_ASCII `\n`
%define TAB_ASCII `\t` 
%define NINE_ASCII 57

 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rcx, rcx

    .loop:
    mov al, byte[rdi + rcx]
    test al, al
    jz .end
    inc rcx
    jmp .loop
    
    .end:
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push    rdi
    call    string_length
    pop     rsi
    mov     rdx, rax
    mov     rax, WRITE_SYSCALL
    mov     rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push    rdi
    mov     rax, WRITE_SYSCALL
    mov     rdi, STDOUT
    mov     rsi, rsp
    mov     rdx, ONE_SYMBOL
    syscall
    pop     rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE_ASCII
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    mov rax, rdi
    mov rcx, TEN_SN
    xor rbx, rbx
    
    .loop:
        xor     rdx, rdx
        div     rcx
        add     rdx, ZERO_ASCII
        push    rdx
        inc     rbx
        test    rax, rax
        jnz     .loop

    .loop_print:
        pop rdi
        push rbx
        call print_char
        pop rbx
        sub rbx, 1
        test rbx, rbx
        jnz .loop_print
    
    pop rbx
    ret
    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    and rdi, rdi
    jns .positive

    .negative:
    push rdi
    mov rdi, MINUS_ASCII
    call print_char
    pop rdi
    neg rdi

    .positive:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor     rcx, rcx
    .loop:
    mov     al, byte[rdi + rcx]
    cmp     al, byte[rsi + rcx]
    jne     .not_equal
    inc     rcx
    test    al, al
    jnz     .loop
    mov     rax, 1
    ret
    
    .not_equal:
    xor     rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov     rax, READ_SYSCALL
    mov     rdi, STDIN
    mov     rdx, 1       
    mov     rsi, rsp
    syscall
    test     al, al
    js      .err
    pop     rax
    ret
    .err:
    mov rax, -1
    pop rdi
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    
    push r12
    push r13
    push r14
    mov r12, rdi ;адрес
    mov r13, rsi ;размер

.check_loop:
    call read_char
    cmp al, TAB_ASCII
    jz .check_loop
    cmp al, SPACE_ASCII
    jz .check_loop
    cmp al, NEWLINE_ASCII
    jz .check_loop
    xor r14, r14
    dec r13

.loop_s:
    cmp al, TAB_ASCII
    jz .done
    cmp al, SPACE_ASCII
    jz .done
    cmp al, NEWLINE_ASCII
    jz .done
    test al, al
    jz .done
    cmp r14, r13
    jnle .err
    mov byte[r12 + r14], al
    inc r14
    call read_char
    jmp .loop_s


.done:
    mov byte[r12 + r14], 0
    mov rax, r12
    mov rdx, r14
    jmp .end

.err:
    xor rax, rax

.end:
    pop r14
    pop r13
    pop r12
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rcx, rcx 
    xor rax, rax
    push r13
    push rbx
    xor r13, r13 
    mov rbx, 10 

    .loop:
    mov r13b, byte[rdi + rcx]
    cmp r13b, ZERO_ASCII
    jl .end
    cmp r13b, NINE_ASCII
    jg .end
    sub r13b, ZERO_ASCII
    add rax, r13 ; add digit and do shift
    mul rbx
    inc rcx    
    jmp .loop

    .end:
    div rbx ; correction
    mov rdx, rcx
    pop rbx
    pop r13
    ret





; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, MINUS_ASCII
    jz .negativ

    call parse_uint
    jmp .end
    
    .negativ:
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    
    .end:
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx

    .loop:
    cmp rax, rdx
    jge .err
    mov cl, byte[rdi + rax]
    cmp cl, STRING_END
    jz .end
    mov byte[rsi + rax], cl
    inc rax
    jmp .loop

    .err:
    xor rax, rax
    ret

    .end:
    mov byte[rsi + rax], STRING_END
    ret
